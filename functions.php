<?php 
function siteStyle () {
    wp_enqueue_style('style', get_stylesheet_uri());
}
add_action('wp_enqueue_scripts', 'siteStyle');
function theme_Jq() {
    
    wp_enqueue_script('jq js', get_template_directory_uri(). '/javaScript/jq.js');

}

add_action('wp_enqueue_scripts', 'theme_Jq');

function theme_javaS() {
    
    wp_enqueue_script('futureGym js', get_template_directory_uri(). '/javaScript/futureGym.js');

}
add_action('wp_enqueue_scripts', 'theme_javaS');

add_action('get_header', 'remove_admin_login_header'); //Remove the 32 PX

function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
/*Theme support*/
add_theme_support('post-thumbnails');

add_image_size('small-thumb', 165, 165, array(left, right));
add_image_size('large-thumb', 500, 250);

function excerpt_length () {
    return 15;
}
add_filter('excerpt_length', 'excerpt_length');
    /****NAV MENUS***/
   register_nav_menus(array(
       'primary' => __('القائمة الرئيسية'),
       'page' => __('قائمة الصفحات الفرعية')
   ));
?>