<?php get_header(); ?>
<title><?php the_title(); ?>|Future Gym</title>
<nav class="onSlide">
   <?php
            $args = array(
                theme_location => 'page'
            )
        ?>
        <?php wp_nav_menu($args); ?>
</nav>
<div class="blog aarchive">
        <H3><?php 
            if(is_category() ) {
                single_cat_title();
            } elseif(is_author() ) {
            the_post();
                echo 'Author Archives: ' . the_post_author(); 
            }
            
            ?></H3>
        <div class="posts">
            <?php
                if (have_posts()) :
                while (have_posts()) : the_post(); ?>
            <div class="post">
                <div class='textPost'>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <span class="post-info"><?php the_time ('d/m/y');?>|
                        <?php 
                                $categories = get_the_category();
                                $saperator = " , ";
                                $output = '';
                                    if($categories) {
                                        forejach ($categories as $category) {
                                            $output .= '<a href=" '. get_category_link($category->term_id) . '">' . $category -> cat_name  . $saperator . '</a>';
                                }
                                        echo trim($output, $saperator);
                                        
                                        
                            }
                        ?> 
                         |<a href="<?php get_the_author_posts(); ?>"><?php the_author(get_the_author_meta("ID")); ?></a> الكاتب </span>
                        <P>
                            <?php echo get_the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>">المزيد</a>
                        </P>
                </div>
                  <span class="small-thumb"><?php the_post_thumbnail('small-thumb'); ?></span>
                <div class="irule"></div>

            </div>
           <?php  endwhile;
            else:
            echo "الصفخة التي تبحث عنها غير موجودة، ربما ترغب في الذهاب إلى الصفحة الرئيسية ";
                endif;?>
      
        </div>
   
</div>
<?php get_footer(); ?>