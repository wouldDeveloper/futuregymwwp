<title><?php the_title(); ?>|Future Gym</title>
<?php get_header(); ?>
<nav class="onSlide">
   <?php
            $args = array(
                theme_location => 'page'
            )
        ?>
        <?php wp_nav_menu($args); ?>
</nav>
<div class="large-thumb">
    <?php the_post_thumbnail('large-thumb'); ?>
</div>
<article class="post-body">
    <h2 class="post-tilt"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
    <P><?php the_time('d/m/y g:i a '); ?> | <?php the_author(); ?></p>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
  the_content(); 
endwhile;
else: ?>
  <p><?php echo('Sorry, no posts matched your criteria.');?> </p> 
<?php endif; ?> 
		</P>
</article>
<?php get_footer(); ?>