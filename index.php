<?php get_header(); ?>
  <nav class="nav-menu">
        <?php
            $args = array(
                theme_location => 'primary'
            )
        ?>
        <?php wp_nav_menu($args); ?>
    </nav>
<?php if(is_home()) {?>
<div class="fullSlider">
    <div class="frontPage slide">
        <div class="slideContent">
            <h2>ابدأ حياتك من جديد</h2>
            <p>نوفهذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. </p>
            <button class="frontButt">المزيد</button>
        </div>
    </div>
    <div class="frontJoin slide">
        <div class="slideContent">
            <h2>ابدأ حياتك من جديد</h2>
            <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.ك </p>
            <button class="frontButt">المزيد</button>
        </div>
    </div>
    <div class="frontCrew slide"> 
        <div class="slideContent">
            <h2>ابدأ حياتك من جديد</h2>
            <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. </p>
            <button class="frontButt">المزيد</button>       
        </div>
    </div>
    <div class="frontPage slide">
        <div class="slideContent">  
            <h2>ابدأ حياتك من جديد</h2>
            <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.ك </p>
            <button class="frontButt">المزيد</button>
        </div>
    </div>
  
    
</div>
<div class="container">
<!------SECTIONS OF GYM START -->
    <div class="divs">
        <h2>أقسام النادي</h2>
        <p>كن جزء من مجتمعنا الرائع، و حسن جسمك</p>
        <div class="cont">
            <div class="spa ">
                <section class="overlay" id='overlay1'></section>
                <div class="info">
                    <p><a>متعة الإستجمام</a></p>
                    <a href="<?php the_permalink(7); ?>">للمزيد..</a>        
                    <h3>القسم المائي</h3>
                </div>            
            </div>
            <Div class='fittnes '>
               <section class="overlay" id="overlay2"></section>
                <div class="info">
                    <p><a>خسارة وزن</a></p>
                    <a href="<?php the_permalink(7); ?>">للمزيد..</a>
                    <h3>أقسام اللياقة</h3>
                </div>           
            </Div>
            <div class="iron ">
                <section class="overlay" id='overlay3'></section>
                <div class="info">
                    <p><a>بناء عضلات</a></p>
                    <a href="<?php the_permalink(7); ?>">للمزيد..</a>
                    <h3> حمل الأوزان</h3>
                </div>           
        </div>
    </div>
</div>
         
    
<!-----SECTIONS OF GYM END---->
<!-----BLOG START---->
    <div class="blog">
        <H3>أنت و اللياقة</H3>
        <p>  اقرأ اّخر النصائح حول كمال الأجسام و نصائخ غذائية و خطط تدريبية عالية المستوى</p>
        <div class="posts">
            <?php
                if (have_posts()) :
                while (have_posts()) : the_post(); ?>
            <div class="post">
                <div class='textPost'>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <span class="post-info"><?php the_time ('d/m/y');?>|
                        <?php 
                                $categories = get_the_category();
                                $saperator = " , ";
                                $output = '';
                                    if($categories) {
                                        foreach ($categories as $category) {
                                            $output .= '<a href=" '. get_category_link($category->term_id) . '">' . $category -> cat_name  . $saperator . '</a>';
                                }
                                        echo trim($output, $saperator);
                                        
                                        
                            }
                        ?> 
                         |<a href="<?php the_permalink(27); ?>"><?php the_author(get_the_author_meta("ID")); ?></a> الكاتب </span>
                        <P>
                            <?php echo get_the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>">المزيد</a>
                        </P>
                </div>
                  <span class="small-thumb"><?php the_post_thumbnail('small-thumb'); ?></span>
                <div class="irule"></div>

            </div>
           <?php  endwhile;
            else:
            echo "الصفخة التي تبحث عنها غير موجودة، ربما ترغب في الذهاب إلى الصفحة الرئيسية ";
                endif;?>
      
        </div>
   
    </div>
    <div class="hajs">
      
    </div>
    <!---ITEMS SLAES--->
    <div class="items-shop">
          <div class="shop-title">
            <h2> متجر النادي </h2><br>
            <p>  جميع المغذيات والأدوات اللازمة لتدريب تجدهها لدينا بأفضل الأسعار</p>
        </div>
        <ul class="saleItems">
            <span class="leftArrow"><i class="fa fa-arrow-left fa-3x"></i></span>
            <li class="first">
                <div class="item">
                </div>
                <div class="infoItem">
                    <h3>Clear Muscle</h3>
                    <span>39.99$</span>
                </div>
            </li>
            <li>
              <div class="item">
                </div>
                <div class="infoItem">
                    <h3>Clear Muscle</h3>
                    <span>39.99$</span>
                </div>
            </li>
            <li>
              <div class="item">
                </div>
                <div class="infoItem">
                    <h3>Clear Muscle</h3>
                    <span>39.99$</span>
                </div>
            </li>
            <li>  <div class="item">
                </div>
                <div class="infoItem">
                    <h3>Clear Muscle</h3>
                    <span>39.99$</span>
                </div>
            </li>
            <span class="rightArrow"> <i class="fa fa-arrow-right fa-3x "></i></span>

            <li>  <div class="item">
                </div>
                <div class="infoItem">
                    <h3>Clear Muscle</h3>
                    <span>39.99$</span>
                </div>
            </li>
        </ul>
    </div>
     
    <div class="crew">
         <div class="crew-title">
            <h2> طاقم العمل </h2><br>
            <p>  كن جزءا من النادي مع مساعدة المحترفين و ذوي الخبرات تكون أقرب لهدفك خطوة</p>
        </div>
        <ul class="crew-st">
            <li>
                <h3>اسم </h3>
                <p>المشرف العام</p>
            </li>
            <li>
                <h3>اسم</h3>
                <p>مدرب اللياقة</p>
            </li>
            <li>
                <h3>اسم</h3>
                <p>مدرب الحديد</p>
            </li>
            <li>
                <h3>اسم</h3>
                <p>مسؤول القسم المائي</p>
            </li>
        </ul>
    </div>
    
   <div class="hajs"></div>
    

<?php } ?>
<?php get_footer(); ?>